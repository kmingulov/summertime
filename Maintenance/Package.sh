# Checking whether the version is given.
if [[ $1 = "" ]]; then
    echo "Usage: $0 version"
    exit
fi

# Working directory.
workdir=`pwd`

# Echoing the destination folder.
echo "Package version:    $1"
echo ""

# Creating directories.
mkdir "/tmp/SummerTime/"
mkdir "/tmp/SummerTime/SummerTime-$1/"

# Copying package files.
echo -n "Copying package files... "
cd ../Source/
find . -name "*.m" | cpio -pdm "/tmp/SummerTime/SummerTime-$1"
find . -name "*.so" | cpio -pdm "/tmp/SummerTime/SummerTime-$1"
echo "Done."

# Copying distribution files.
echo -n "Copying distribution files... "
cp ../README.md "/tmp/SummerTime/"
cp ../DistributionFiles/* "/tmp/SummerTime/"
echo "Done."

# Creating launcher file.
echo -n "Creating launcher... "
echo "SetDirectory@DirectoryName@\$InputFileName; << \"SummerTime-$1/SummerTime.m\"; ResetDirectory[];" > "/tmp/SummerTime/SummerTime.m"
echo "Done."

# Creating the archive.
echo -n "Creating the archive... "
cd "/tmp"
zip -r "$workdir/../$1.zip" "SummerTime/"
echo "Done."

# Removing files.
rm -rf "/tmp/SummerTime/"

# Everything's done.
echo ""
echo "Everything is done."