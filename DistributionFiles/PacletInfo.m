Paclet[
	Name -> "SummerTime",
	Version -> "1.2",
	MathematicaVersion -> "10+",
	Description -> "SummerTime package is designed for calculations of nested sums and their expansions. Package also contains several applications, which can be useful, as, for example, functions for calculating multiple zeta values (MZV).",
    Creator -> "Roman N. Lee, Kirill T. Mingulov",
	Extensions -> {
		{
			"Kernel",
			"Context" -> {"SummerTime`"}
		}
	}
]
