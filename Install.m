(* ::Package:: *)

(* SUMMERTIME INSTALLATION SCRIPT *)


Module[{installProjectInstaller},

Needs["Utilities`URLTools`"];

(* Script for ProjectInstaller installation. *)
(* Original script at https://github.com/lshifr/ProjectInstaller can fail with SSL error. *)
(* Our forked version (with different download URL) works fine. *)
installProjectInstaller := Module[{$DownloadUrl, dir, files, root, result},
  $DownloadUrl = "https://raw.githubusercontent.com/lshifr/ProjectInstaller/master/Release/ProjectInstaller.zip";

  dir = FileNameJoin[{$UserBaseDirectory, "Applications","ProjectInstaller"}];
  If[DirectoryQ[dir], DeleteDirectory[dir, DeleteContents -> True]];
  files = ExtractArchive[Utilities`URLTools`FetchURL[$DownloadUrl]];
  root = Cases[files, r_ /; StringMatchQ[r, __ ~~ "ProjectInstaller"]];
  If[Length[root] =!= 1, result = $Failed, root = First@root];
  If[result === $Failed,
  	$Failed, 
  	(* else *) 
  	dir = CopyDirectory[root, dir];
  	DeleteDirectory[root, DeleteContents -> True];
  	dir
  ]
 ];

(* SummerTime installation script. *)
Module[{summertime, objects, oldHeaders, errors = False, json, file, path},

	(* Formatted names. *)
	summertime="\!\(\*StyleBox[\"SummerTime\",FontWeight->\"Bold\",FontColor->RGBColor[0,0.444444,0]]\)";
	(* Printing hello-message. *)
	Print["Installing ", summertime, "."];

	(* Checking for ProjectInstaller. *)
	If[Quiet[Needs["ProjectInstaller`"]] === $Failed,
		(* ===== PI NOT FOUND ===== *)
		Print["\t- Installing \!\(\*StyleBox[\"ProjectInstaller\",FontWeight->\"Bold\"]\) by L. Shifrin."];
		installProjectInstaller;
		Needs["ProjectInstaller`"];,

		(* ===== PI FOUND ===== *)
		Print["\t- Using \!\(\*StyleBox[\"ProjectInstaller\",FontWeight->\"Bold\"]\) by L. Shifrin."];
	];

	(* Modifying HTTP headers, used by URLTools functions. *)
	(* This fix was taken from BootstrapInstaller by jkuczm, licensed under MIT: *)
	(*   https://github.com/jkuczm/MathematicaBootstrapInstaller *)
	oldHeaders = OptionValue[Utilities`URLTools`Private`FetchURLInternal,
		"RequestHeaderFields"
	];
	SetOptions[Utilities`URLTools`Private`FetchURLInternal,
		"RequestHeaderFields" -> {"Content-Type" -> ""}
	];

	(* Installing the package. *)
	CheckAbort[
		(* Getting the latest version. *)
		Check[
			json = Import["https://api.bitbucket.org/2.0/repositories/kmingulov/summertime/downloads","JSON"],
			Print["Failed to connect to the remote repository!"];
			Abort[];
		];
		json = "values" /. json;
		If[Length@json == 0,
			Print["The remote repository is empty, cannot download ", summertime, "!"];
			Abort[];
		];
		file = "name" /. json[[1]];
		file = "https://bitbucket.org/kmingulov/summertime/downloads/" <> file;

		(* Installing. *)
		Print["\t- Installing ", summertime, " from: ", file];
		Quiet[path = ProjectInstaller`ProjectInstall[URL[file]]];
		If[path === $Failed,
			Print["\t- Uninstalling the previous version."];
			ProjectInstaller`ProjectUninstall["SummerTime"];
			path = ProjectInstaller`ProjectInstall[URL[file]];
		];

		(* Done. *)
		Print["Installation of ", summertime, " is complete. You can load the package by <<SummerTime`."];
		Print["Package is located at: ", path, "."];,

		(* Error occured. *)
		errors = True;
	];

	(* Setting headers back. *)
	SetOptions[Utilities`URLTools`Private`FetchURLInternal,
		"RequestHeaderFields" -> oldHeaders
	];

	(* Was there an error? Printing information. *)
	If[errors,
		Print["\!\(\*StyleBox[\"FAILED!\",FontWeight->\"Bold\",FontColor->RGBColor[1,0,0]]\) Please, try to install ", summertime, " manually."]
	];
];

];