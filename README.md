SummerTime
==========

**SummerTime** is a *Wolfram Mathematica* package performing the high-precision calculation of the multiple sums with factorized summand.

If you are interested in using **SummerTime** in multiloop computations, please have a look at [**DREAM**](https://bitbucket.org/kmingulov/dream/), a successor of **SummerTime**.

**SummerTime** is a free software and is distributed under the terms of GNU GPL v2.

*Official website:* [www.inp.nsk.su/~lee/programs/SummerTime/](http://www.inp.nsk.su/~lee/programs/SummerTime/)

*Official repository:* [bitbucket.org/kmingulov/summertime/](https://bitbucket.org/kmingulov/summertime/)

*Version:* 1.2 (4 December 2017)

*Authors:* Kirill T. Mingulov & Roman N. Lee,
Budker Institute of Nuclear Physics, Novosibirsk

***

## Requirements

**SummerTime** requires:

* Wolfram Mathematica (version 10 or higher)
* [GMP library](https://gmplib.org/) and GNU OpenMP (optional --- for better performance in case of convergence acceleration) 

***

## Installation
There are two ways to install **SummerTime**.

### 1. Automatic online installation
You can install **SummerTime** using our installation script. Type in *Mathematica* session the following command:

```mathematica
Import["https://bitbucket.org/kmingulov/summertime/raw/master/Install.m"];
```

It will download the latest release of **SummerTime** and install it. After that, you can load the package by the following command:

```mathematica
<<SummerTime`
```

### 2. Manual installation
Download the archive with the latest version of the package from the [downloads page](https://bitbucket.org/kmingulov/summertime/downloads) of our repository. Follow instructions in *INSTALLATION NOTES.txt* file from the archive.

***

## Bugs and feature requests
If you experience any problems with the package or have encountered a bug or just want something to be added into the package, please, leave an issue on our [issue tracker](https://bitbucket.org/kmingulov/summertime/issues?status=new&status=open).