(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



Begin["`Private`"];


{CalculateAsymptotics};


ReduceConvexHull[h_]:=Block[{hull,k,m,MinX,MaxX,MinY,MaxY},
(* Adding marks, whether point will be in result array or not. *)
hull={#1,#2,True}&@@@h;
(* Iterating over all pairs of points. *)
Do[
(* Finding a line equation. *)
{k,m}=LineEquation[hull[[j,1;;2]],hull[[i,1;;2]]];
(* Minimal and maximal values. *)
MinX=Min[hull[[i,1]],hull[[j,1]]];MaxX=Max[hull[[i,1]],hull[[j,1]]];
MinY=Min[hull[[i,2]],hull[[j,2]]];MaxY=Max[hull[[i,2]],hull[[j,2]]];
(* Checking all other points. *)
Do[
If[l!=i&&l!=j&&hull[[l,2]]==k*hull[[l,1]]+m&&MinX<=hull[[l,1]]<=MaxX&&MinY<=hull[[l,2]]<=MaxY,
hull[[l,3]]=False
],
{l,1,Length@hull}
],
{i,1,Length@hull-1},{j,i+1,Length@hull}
];
(* Result. *)
{#1,#2}&@@@Cases[hull,{_,_,True}]
];


PolygonUpperEdges[points_]:=Block[{m,k,res={}},
(* Iterating over all pairs of points. *)
Do[
(* Calculating equation of a line. *)
{k,m}=LineEquation[points[[j]],points[[i]]];
(* Check that the rest points lie under that line. *)
If[And@@((#2(*y*)-k #1(*x*)-m<=0&)@@@Delete[points,{{i},{j}}]),
AppendTo[res,points[[i]]->points[[j]]]
],
{i,1,Length@points-1},{j,i+1,Length@points}
];
(* Done. *)
res
];


TulyakovAsymptotics[diffeqi_,var_,n_,q_]:=Block[{diffeq,d,dt,d2dt,dt2d,coefs,points,hull,edges,xs,ms,bs,polynomials,\[Lambda]s,asymp,sol,eq,var0,var1},
(*******************************************************************************)
(* 1. FINDING LEADING ASYMPTOTICS **********************************************)
(*******************************************************************************)

(* Getting rid of denominator, if there is any. *)
diffeq=Collect[diffeqi*Denominator[Together@diffeqi],_q,Simplify];
(* Coefficients of difference equation. *)
coefs=Table[Coefficient[diffeq,q[var+i]],{i,0,n}];
(* Creating a list of points. *)
points={#,Exponent[coefs[[#]],var]}&/@Range[1,n+1];
(* Checking whether they all lie on the same line. *)
If[
!Block[{k,m,r=True},
{k,m}=LineEquation[points[[1]],points[[2]]];
Do[If[points[[i,2]]!=k*points[[i,1]]+m,r=False;Break[]],{i,3,Length@points}];
r
],
(* === ALL POINTS DON'T LIE ON THE SAME LINE === *)
(* First of all, calculating convex hull of these points. Result is BoundaryMeshRegion; its coordinates can be obtained by MeshCoordinates. *)
hull=ConvexHullMesh[points];
hull=Rationalize@MeshCoordinates@hull;
(* Performing reduction, see note for ReduceConvexHull function. *)
hull=ReduceConvexHull[hull];
(* Now we need to get the subset with upper edges. *)
edges=PolygonUpperEdges@hull;
(* We've got edges, but we need only points. *)
edges=Sort@DeleteDuplicates[edges/.Rule->Sequence],
(* === ALL POINTS LIE ON THE SAME LINE === *)
(* In this case it's not necessary to calculate convex hull: it's just the same set of points. Though we need to reduce it. *)
hull=ReduceConvexHull[points];
(* Vertices of our edges than are: *)
edges=hull;
];
(* Now we calculate auxiliary variables. *)
xs=First/@edges;
ms=xs[[2;;Length@xs]]-xs[[1;;Length@xs-1]];
bs=1/ms ((Exponent[coefs[[#]],var]&/@xs[[2;;Length@xs]])-(Exponent[coefs[[#]],var]&/@xs[[1;;Length@xs-1]]));
(* Constructing polynomials and their solutions. *)
polynomials=Table[
Sum[var^k*Coefficient[coefs[[xs[[j]]+k]],var,Exponent[coefs[[xs[[j]]]],var]+k bs[[j]]],{k,0,ms[[j]]}],
{j,1,Length@ms}
];
\[Lambda]s=Table[
Solve[polynomials[[j]]==0,var],
{j,1,Length@ms}
];
\[Lambda]s=(\[Lambda]s/.Rule->Sequence)[[All,All,2]];
(* Leading asymptotics of all solutions. *)
asymp=Flatten[var^-bs*\[Lambda]s];
(* Getting the "fastest" asymptotics. *)
sol=Last@Sort@asymp;

(*******************************************************************************)
(* 2. SEPARATING MULTIPLE SOLUTIONS ********************************************)
(*******************************************************************************)

(* Checking, whether our solution is multiple. *)
If[Length@Cases[asymp,sol]==1,
(* If no, doing nothing. *)
Null,

(* If yes, separating them and taking the "fastest". *)
(* TODO *)
Print["TODO: multiple solutions!"];
];

(*******************************************************************************)
(* 3. CONSTRUCTING MORE PRECISE ASYMPTOTICS ************************************)
(*******************************************************************************)

(* Passing to form via d. *)
eq=diffeq;
Do[
eq=eq/.q[var+i]->q[var+i-1]*d[var+i-1],
{i,n,1,-1}
];
eq=eq/.q[var]->1;

(* Substituing leading asymptotics. *)
eq=eq/.d[z_]:>(sol/.var->z)*d[z];

(* Passing to a special form of the difference equation. *)
dt2d=Table[
dt[i]->Sum[(-1)^(i-s) Binomial[i,s]Product[d[var+k-1],{k,1,s}],{s,0,i}],
{i,1,3}
];
d2dt=First@Solve[
Equal@@@dt2d,
d[var+#]&/@Range[0,2]
];
eq=eq/.d2dt;
eq=Collect[eq,_dt,Simplify];

(* Finally, getting the asymptotics. *)
sol=sol*Simplify[(1-(eq/.dt[1]->0)/Coefficient[eq,dt[1]])/.dt2d/._d->1];
sol=Normal@SmartSeries[sol,{var,\[Infinity],2}];

(*******************************************************************************)
(* 4. CONSTRUCTING ASYMPTOTICS FOR q *******************************************)
(*******************************************************************************)

sol=Product[sol,{var,var0,var1}];
sol=CalculateCertificate[sol/.var1->var,var];
sol=Normal@SmartSeries[sol/.var1->var,{var,\[Infinity],2}];
CalculateAsymptotics[sol,var]
]


CalculateAsymptotics::usage="CalculateAsymptotics[\!\(\*
StyleBox[\"cert\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"n\",\nFontSlant->\"Italic\"]\)] finds asymptotics of the expression, given by its certificate \!\(\*
StyleBox[\"cert\",\nFontSlant->\"Italic\"]\)\!\(\*
StyleBox[\",\",\nFontSlant->\"Italic\"]\) as \!\(\*
StyleBox[\"n\",\nFontSlant->\"Italic\"]\) approaches to positive infinity. Certificate \!\(\*
StyleBox[\"cert\",\nFontSlant->\"Italic\"]\) can be either scalar or matrix.

CalculateAsymptotics returns a list of three number {q, \[Alpha], \[Beta]}, which mean that expression's asymptotics is \!\(\*FractionBox[SuperscriptBox[\"q\", 
StyleBox[\"n\",\nFontSlant->\"Italic\"]], \(\*SuperscriptBox[
StyleBox[\"n\",\nFontSlant->\"Italic\"], \"\[Alpha]\"] \*SuperscriptBox[
RowBox[{\"(\", 
RowBox[{
StyleBox[\"n\",\nFontSlant->\"Italic\"], \"!\"}], \")\"}], \"\[Beta]\"]\)]\).";


CalculateAsymptotics::Failed="Failed to find asymptotics.";


CalculateAsymptotics[m_?SquareMatrixQ,var_]:=Block[{diffeq,q},
diffeq=MatrixToDifferenceEquation[m,var,q];
TulyakovAsymptotics[diffeq,var,First[Dimensions@m],q]
];


CalculateAsymptotics[m_?MatrixQ,var_]:=First@MaximalBy[
CalculateAsymptotics[#,var]&/@Flatten[m],
Abs@#[[1]]&
];


CalculateAsymptotics[cert_,var_]/;(Head@cert=!=List):=Block[{fcert,sd,q0,\[Alpha]0,\[Beta]0},
(* ===== CALCULATING SERIES EXPANSION ===== *)
If[FreeQ[cert,var],
(* ===== Constant certificates ===== *)
{q0,\[Alpha]0,\[Beta]0}={cert,0,0},

(* ===== Non-constant certificates ===== *)
(* Getting the leading power. *)
fcert=Factor[cert/.var->1/var];
sd=Series[fcert,{var,0,0}];
(* Checking whether series data are correct. *)
TrueOrAbort[
MatchQ[sd,_SeriesData],
Message[CalculateAsymptotics::Failed]
];
(* At last, calculating the series expansion. *)
sd=Series[fcert,{var,0,sd[[4]]+1}];
(* Calculating q0, \[Alpha]0 and \[Beta]0. *)
q0=SeriesCoefficient[sd,\[Beta]0];
\[Alpha]0=-SeriesCoefficient[sd,\[Beta]0+1]/q0;
\[Beta]0=sd[[4]];
(* Simplifying them (they can be complex). *)
{q0,\[Alpha]0,\[Beta]0}={q0,\[Alpha]0,\[Beta]0};
{q0,\[Alpha]0,\[Beta]0}=ComplexExpand@{q0,\[Alpha]0,\[Beta]0};
];

(* ===== PERFORMING CHECK ===== *)
FalseOrAbort[
Or@@MatchQ[_?(!NumericQ[#]&)]/@{q0,\[Alpha]0,\[Beta]0},
Message[CalculateAsymptotics::Failed]
];

(* ===== DONE ===== *)
{q0,\[Alpha]0,\[Beta]0}
];


End[];
