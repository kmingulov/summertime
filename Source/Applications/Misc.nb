Notebook[{

Cell[CellGroupData[{
Cell["Miscellaneous constants", "Title"],

Cell["\<\
This notebook implementation of several transcendental constans, which appear \
in loop calculations.\
\>", "Text"],

Cell[CellGroupData[{

Cell["Change log", "Section"],

Cell[TextData[{
 StyleBox["August 6, 2015.",
  FontWeight->"Bold"],
 " Initial version, based on R.N. Lee\[CloseCurlyQuote]s code from ",
 StyleBox["MiscFuncs.nb",
  FontSlant->"Italic"],
 " file. Also ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["s", "6"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " is added."
}], "Item"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Preamble", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{"Chi5", ",", "Chi5Tilde", ",", "s6"}], "}"}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Begin", "[", "\"\<`Private`\>\"", "]"}], ";"}]], "Code"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Messages", "Section"],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox["\[Chi]", "5"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Chi5", "::", "usage"}], "=", 
   "\"\<Chi5 represents transcendental number \!\(\*SubscriptBox[\(\[Chi]\), \
\(5\)]\). Its numerical value is 0.067827\[Ellipsis]\>\""}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox[
   OverscriptBox["\[Chi]", "~"], "5"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Chi5Tilde", "::", "usage"}], "=", 
   "\"\<Chi5Tilde represents transcendental number \
\!\(\*SubscriptBox[OverscriptBox[\(\[Chi]\), \(~\)], \(5\)]\). Its numerical \
value is 0.49517\[Ellipsis]\>\""}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox["s", "6"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"s6", "::", "usage"}], "=", 
   "\"\<s6 represents transcendental number \!\(\*SubscriptBox[\(s\), \
\(6\)]\). Its numerical value is 0.98744\[Ellipsis]\>\""}], ";"}]], "Code"]
}, Closed]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Traditional form", "Section"],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox["\[Chi]", "5"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"MakeBoxes", "[", 
    RowBox[{"Chi5", ",", "TraditionalForm"}], "]"}], "^:=", 
   "\[IndentingNewLine]", 
   RowBox[{"InterpretationBox", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"SubscriptBox", "[", 
      RowBox[{"\"\<\[Chi]\>\"", ",", "\"\<5\>\""}], "]"}], ",", 
     "\[IndentingNewLine]", "Chi5", ",", "\[IndentingNewLine]", 
     RowBox[{"Editable", "\[Rule]", "False"}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox[
   OverscriptBox["\[Chi]", "~"], "5"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"MakeBoxes", "[", 
    RowBox[{"Chi5Tilde", ",", "TraditionalForm"}], "]"}], "^:=", 
   "\[IndentingNewLine]", 
   RowBox[{"InterpretationBox", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"SubscriptBox", "[", 
      RowBox[{
       RowBox[{"OverscriptBox", "[", 
        RowBox[{"\"\<\[Chi]\>\"", ",", "\"\<~\>\""}], "]"}], ",", 
       "\"\<5\>\""}], "]"}], ",", "\[IndentingNewLine]", "Chi5Tilde", ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Editable", "\[Rule]", "False"}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox["s", "6"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"MakeBoxes", "[", 
    RowBox[{"s6", ",", "TraditionalForm"}], "]"}], "^:=", 
   "\[IndentingNewLine]", 
   RowBox[{"InterpretationBox", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"SubscriptBox", "[", 
      RowBox[{"\"\<s\>\"", ",", "\"\<6\>\""}], "]"}], ",", 
     "\[IndentingNewLine]", "s6", ",", "\[IndentingNewLine]", 
     RowBox[{"Editable", "\[Rule]", "False"}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}]], "Code"]
}, Closed]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Definitions for N", "Section"],

Cell[CellGroupData[{

Cell["Unprotecting N", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", "N", "]"}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox["\[Chi]", "5"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Verbatim", "[", "N", "]"}], "[", 
    RowBox[{"Chi5", ",", 
     RowBox[{"{", 
      RowBox[{"p_", ",", "a_"}], "}"}]}], "]"}], ":=", 
   RowBox[{"N", "[", 
    RowBox[{"Chi5", ",", 
     RowBox[{"Min", "@", 
      RowBox[{"{", 
       RowBox[{"p", ",", "a"}], "}"}]}]}], "]"}]}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"N", "[", 
    RowBox[{"Chi5", ",", 
     RowBox[{"p_", "?", "NumericQ"}]}], "]"}], ":=", 
   RowBox[{"TreeSums", "[", 
    RowBox[{
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"6", 
          FractionBox["1", 
           RowBox[{"m", "-", "3"}]], 
          FractionBox["1", 
           RowBox[{"l", "-", "2"}]], 
          FractionBox["1", 
           RowBox[{"k", "-", "1"}]], 
          FractionBox[
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"n", "!"}], ")"}], "2"], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"4", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", 
              RowBox[{"l", ",", 
               RowBox[{"{", 
                RowBox[{"k", ",", 
                 RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], 
          "}"}]}], "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", 
          FractionBox["1", 
           RowBox[{"m", "-", "2"}]], 
          FractionBox["1", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"k", "-", "1"}], ")"}], "2"]], 
          FractionBox[
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"n", "!"}], ")"}], "2"], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"3", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", 
              RowBox[{"k", ",", 
               RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], "}"}], 
       ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", 
          FractionBox["1", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"m", "-", "2"}], ")"}], "2"]], 
          FractionBox["1", 
           RowBox[{"(", 
            RowBox[{"k", "-", "1"}], ")"}]], 
          FractionBox[
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"n", "!"}], ")"}], "2"], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"3", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", 
              RowBox[{"k", ",", 
               RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], "}"}], 
       ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          FractionBox["1", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"m", "-", "1"}], ")"}], "3"]], 
          FractionBox[
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"n", "!"}], ")"}], "2"], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"2", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], 
      "\[IndentingNewLine]", "}"}], ",", "\[IndentingNewLine]", "p", ",", 
     RowBox[{"Information", "\[Rule]", 
      RowBox[{"TraditionalForm", "@", "Chi5"}]}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox[
   OverscriptBox["\[Chi]", "~"], "5"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Verbatim", "[", "N", "]"}], "[", 
    RowBox[{"Chi5Tilde", ",", 
     RowBox[{"{", 
      RowBox[{"p_", ",", "a_"}], "}"}]}], "]"}], ":=", 
   RowBox[{"N", "[", 
    RowBox[{"Chi5Tilde", ",", 
     RowBox[{"Min", "@", 
      RowBox[{"{", 
       RowBox[{"p", ",", "a"}], "}"}]}]}], "]"}]}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"N", "[", 
    RowBox[{"Chi5Tilde", ",", 
     RowBox[{"p_", "?", "NumericQ"}]}], "]"}], ":=", 
   RowBox[{"TreeSums", "[", 
    RowBox[{
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"6", 
          FractionBox["1", 
           RowBox[{"m", "-", "3"}]], 
          FractionBox["1", 
           RowBox[{"l", "-", "2"}]], 
          FractionBox["1", 
           RowBox[{"k", "-", "1"}]], 
          FractionBox[
           RowBox[{
            SuperscriptBox[
             RowBox[{"(", 
              RowBox[{"n", "!"}], ")"}], "2"], 
            SuperscriptBox["2", "n"]}], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"4", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", 
              RowBox[{"l", ",", 
               RowBox[{"{", 
                RowBox[{"k", ",", 
                 RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], 
          "}"}]}], "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", 
          FractionBox["1", 
           RowBox[{"m", "-", "2"}]], 
          FractionBox["1", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"k", "-", "1"}], ")"}], "2"]], 
          FractionBox[
           RowBox[{
            SuperscriptBox[
             RowBox[{"(", 
              RowBox[{"n", "!"}], ")"}], "2"], 
            SuperscriptBox["2", "n"]}], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"3", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", 
              RowBox[{"k", ",", 
               RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], "}"}], 
       ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", 
          FractionBox["1", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"m", "-", "2"}], ")"}], "2"]], 
          FractionBox["1", 
           RowBox[{"(", 
            RowBox[{"k", "-", "1"}], ")"}]], 
          FractionBox[
           RowBox[{
            SuperscriptBox[
             RowBox[{"(", 
              RowBox[{"n", "!"}], ")"}], "2"], 
            SuperscriptBox["2", "n"]}], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"3", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", 
              RowBox[{"k", ",", 
               RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], "}"}], 
       ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          FractionBox["1", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"m", "-", "1"}], ")"}], "3"]], 
          FractionBox[
           RowBox[{
            SuperscriptBox[
             RowBox[{"(", 
              RowBox[{"n", "!"}], ")"}], "2"], 
            SuperscriptBox["2", "n"]}], 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"2", "n"}], ")"}], "!"}], " ", 
            SuperscriptBox["n", "2"]}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"2", ",", 
           RowBox[{"{", 
            RowBox[{"m", ",", 
             RowBox[{"{", "n", "}"}]}], "}"}]}], "}"}]}], "}"}]}], 
      "\[IndentingNewLine]", "}"}], ",", "\[IndentingNewLine]", "p", ",", 
     RowBox[{"Information", "\[Rule]", 
      RowBox[{"TraditionalForm", "@", "Chi5Tilde"}]}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 FormBox[
  SubscriptBox["s", "6"], TraditionalForm]],
 FormatType->"TraditionalForm"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Verbatim", "[", "N", "]"}], "[", 
    RowBox[{"s6", ",", 
     RowBox[{"{", 
      RowBox[{"p_", ",", "a_"}], "}"}]}], "]"}], ":=", 
   RowBox[{"N", "[", 
    RowBox[{"s6", ",", 
     RowBox[{"Min", "@", 
      RowBox[{"{", 
       RowBox[{"p", ",", "a"}], "}"}]}]}], "]"}]}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"N", "[", 
    RowBox[{"s6", ",", 
     RowBox[{"p_", "?", "NumericQ"}]}], "]"}], ":=", 
   RowBox[{"TreeSum", "[", 
    RowBox[{
     FractionBox[
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"-", "1"}], ")"}], 
       RowBox[{"m", "+", "k"}]], 
      RowBox[{
       SuperscriptBox["m", "5"], "k"}]], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", 
       RowBox[{"{", 
        RowBox[{"k", ",", 
         RowBox[{"{", "m", "}"}]}], "}"}]}], "}"}], ",", "p", ",", 
     RowBox[{"Information", "\[Rule]", 
      RowBox[{"TraditionalForm", "@", "s6"}]}]}], "]"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Protecting N", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "N", "]"}], ";"}]], "Code"]
}, Closed]]
}, Open  ]],

Cell[CellGroupData[{

Cell["End", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"End", "[", "]"}], ";"}]], "Code"]
}, Open  ]]
}, Open  ]]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1366, 685},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
PrivateNotebookOptions->{"FileOutlineCache"->False},
ShowCellTags->True,
TrackCellChangeTimes->False,
Magnification:>1.25 Inherited,
FrontEndVersion->"10.0 for Microsoft Windows (64-bit) (December 4, 2014)",
StyleDefinitions->FrontEnd`FileName[{
   ParentDirectory[]}, "Stylesheet.nb", CharacterEncoding -> "WindowsCyrillic"]
]

